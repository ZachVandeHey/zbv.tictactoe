#pragma once
#include <iostream>
//#include <conio.h>

using namespace std;

class TicTacToe
{

private:

	char m_board[9] = { '1','2','3','4','5','6','7','8','9' };

	int m_numTurns =0;

	char m_playerTurn;

public:

	void DisplayBoard()
	{
		for (int i = 0; i < 9; i++)
		{
			cout << m_board[i] << "\t";
			if (i == 2 || i == 5 || i == 8)
				cout << "\n";
		}
	}

	bool IsOver()
	{
		bool Over = false;
		if (m_board[0] == 'X' && m_board[1] == 'X' &&m_board[2] == 'X') {
			Over = true;
		}
		if (m_board[0] == 'O' && m_board[1] == 'O' &&m_board[2] == 'O') {
			Over = true;
		}
		if (m_board[3] == 'X' && m_board[4] == 'X' &&m_board[5] == 'X') {
			Over = true;
		}
		if (m_board[3] == 'O' && m_board[4] == 'O' &&m_board[5] == 'O') {
			Over = true;
		}
		if (m_board[6] == 'X' && m_board[7] == 'X' &&m_board[8] == 'X') {
			Over = true;
		}
		if (m_board[6] == 'O' && m_board[7] == 'O' &&m_board[8] == 'O') {
			Over = true;
		}
		if (m_board[0] == 'X' && m_board[3] == 'X' &&m_board[6] == 'X') {
			Over = true;
		}
		if (m_board[0] == 'O' && m_board[3] == 'O' &&m_board[6] == 'O') {
			Over = true;
		}
		if (m_board[1] == 'X' && m_board[4] == 'X' &&m_board[7] == 'X') {
			Over = true;
		}
		if (m_board[1] == 'O' && m_board[4] == 'O' &&m_board[7] == 'O') {
			Over = true;
		}
		if (m_board[2] == 'X' && m_board[5] == 'X' &&m_board[8] == 'X') {
			Over = true;
		}
		if (m_board[2] == 'O' && m_board[5] == 'O' &&m_board[8] == 'O') {
			Over = true;
		}
		if (m_board[0] == 'X' && m_board[4] == 'X' &&m_board[8] == 'X') {
			Over = true;
		}
		if (m_board[0] == 'O' && m_board[4] == 'O' &&m_board[8] == 'O') {
			Over = true;
		}
		if (m_board[2] == 'X' && m_board[4] == 'X' &&m_board[6] == 'X') {
			Over = true;
		}
		if (m_board[2] == 'O' && m_board[4] == 'O' &&m_board[6] == 'O') {
			Over = true;
		}
		if (m_numTurns == 9) {
			Over = true;
		}
		return Over;
	}

	char GetPlayerTurn()
	{
		if (m_playerTurn == 'X')
			m_playerTurn = 'O';
		else
			m_playerTurn = 'X';
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		bool IsValidMove = true;

		if (m_board[position - 1] == 'X' or m_board[position - 1] == 'O' or position > 9 or position < 1)  
		{
			IsValidMove = false;
		}

		return IsValidMove;
	}

	void Move(int position)
	{
		m_board[position -1] = m_playerTurn;
		m_numTurns += 1;
	}

	void DisplayResult()
	{
		if (m_numTurns == 9) {
			cout << "The game is a tie\n";
		}
		else {
			cout << "The winner is: " << m_playerTurn << "\n";
		}
		
	}

};
